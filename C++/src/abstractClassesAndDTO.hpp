#ifndef INTERFACESANDDTO_H
#define INTERFACESANDDTO_H

#include <iostream>
#include <vector>
#include <list>

#define TOLERANCE_PARALLELISM 10e-5
#define TOLERANCE_DISTANCE 10e-8

using namespace std;

namespace abstractClassesAndDTO {

    enum SideOfVertex {
        Left = 0,
        Right = 1,
        OnLine = 2
    };

    class Vertex {
    public:
      double X, Y;
      int Id;
      Vertex(double x = 0., double y = 0., int id = 0) {
          X=x;
          Y=y;
          Id=id;
      }

      friend bool operator==(const Vertex& lhs, const Vertex& rhs) {
          if(abs(lhs.X-rhs.X)<TOLERANCE_DISTANCE) {
              if(abs(lhs.Y-rhs.Y)<TOLERANCE_DISTANCE) {
                  return true;
              }
          }
          return false;
      }
    };

    class VertexPointer {
    public:
        //Attributi utili per caso concavo
        int Id, PrevPos, NextPos;
        bool Visited;
        double ParCoordOnLine;

        VertexPointer(int id = 0) {
            Id = id;
            NextPos = 0;
            PrevPos = 0;
            Visited = false;
            ParCoordOnLine = 0.;
        }
    };

    class Segment {
    public:
        Vertex First, Second;
        Segment(Vertex first = Vertex(), Vertex second = Vertex()) {
            First=first;
            Second=second;
        }
    };

    class InputProblemData {
    public:
        Segment SplitLine;
        vector<Vertex> Polygon;
        InputProblemData() {};
    };

    class OutputProblemData {
    public:
        vector<vector<Vertex>> Polygons;
        vector<Vertex> Vertices;
        OutputProblemData() {};

        friend bool operator==(const OutputProblemData& lhs, const OutputProblemData& rhs) {
            if(lhs.Polygons.size()!=rhs.Polygons.size() || lhs.Vertices.size()!=rhs.Vertices.size())
                return false;
            else {
                for(unsigned int i = 0; i < lhs.Polygons.size(); i++) {
                    if(lhs.Polygons[i].size()!=rhs.Polygons[i].size())
                        return false;
                    for(unsigned int v = 0; v < lhs.Polygons[i].size(); v++) {
                        if(!(lhs.Polygons[i][v]==rhs.Polygons[i][v]))
                            return false;
                    }
                }

            }
            return true;
        }
    };



}

#endif // INTERFACESANDDTO_H
