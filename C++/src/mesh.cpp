#include "mesh.hpp"

namespace mesh {

double Polygon::ComputeArea() const {
    vector<Vertex> matVertices = Vertices;
    matVertices.push_back(Vertices[0]);
    double sumForward = 0;
    for (unsigned int i=0; i < matVertices.size()-1; i++)
        sumForward += matVertices[i].X * matVertices[i+1].Y;
    double sumBack = 0;
    for (unsigned int i=0; i < matVertices.size()-1; i++)
        sumBack += matVertices[i].Y * matVertices[i+1].X;
    double area = 0.5 * abs(sumForward - sumBack);
    return area;

}

BoundingBox Polygon::ComputeBoundingBox() const {
    double minX, minY, maxX, maxY;
    minX=Vertices[0].X;
    maxX=minX;
    minY=Vertices[0].Y;
    maxY=minY;

    int polySize = Vertices.size();

    for(int i = 1; i < polySize; i++) {
        const double& currentX = Vertices[i].X;
        const double& currentY = Vertices[i].Y;
        if(currentX<minX)
            minX=currentX;
        else if(currentX>maxX)
            maxX=currentX;
        if(currentY<minY)
            minY=currentY;
        else if(currentY>maxY)
            maxY = currentY;
    }

    BoundingBox output;
    output.NE=Vertex(maxX,maxY,polySize+3);
    output.NW=Vertex(minX,maxY,polySize);
    output.SW=Vertex(minX,minY,polySize+1);
    output.SE=Vertex(maxX,minY,polySize+2);

    return output;
}

void Polygon::AddIntermediatePoints(vector<Vertex> points)
{
    int numPoints = points.size();
    int polySize = Vertices.size();

    for(int i = 0; i < polySize; i++) {
        Vertex current = Vertices[i];
        Vertex next = Vertices[i+1%polySize];
        Segment edge(current,next);
        for(int p = 0; p < numPoints; p++) {
            if(AbstractPolygonCutter::GetVertexSide(edge,points[p])==SideOfVertex::OnLine) {
                double parCoord = AbstractPolygonCutter::GetParametricCoordinate(edge,points[p]);
                if(parCoord>TOLERANCE_DISTANCE && parCoord<1-TOLERANCE_DISTANCE) {
                    Vertices.insert(Vertices.begin()+i+1,points[p]);
                    i++;
                }
                break;
            }
        }
    }
}

void Polygon::TraslateVertices(double traslX, double traslY)
{

    //devo traslare i vertici prima di inserirli in meshPolygons
    for (int v = 0; v < Vertices.size(); v++){
        Vertices[v].X += traslX;
        Vertices[v].Y += traslY;
    }
}

}
