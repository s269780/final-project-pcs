#ifndef MESH_H
#define MESH_H

#include "polygonCutter.hpp"
#include <vector>

using namespace std;
using namespace polygonCutter;

namespace mesh {

    class BoundingBox {
    public:
        Vertex NW, SW, SE, NE;
    };

    class Polygon {
    public:
        vector<Vertex> Vertices;
        Polygon(vector<Vertex> vertices = {}) {
            Vertices=vertices;
        }
        double ComputeArea() const;
        BoundingBox ComputeBoundingBox() const;
        void AddIntermediatePoints(vector<Vertex> points);
        void TraslateVertices(double traslX, double traslY);
    };

    class ReferenceElement {
    public:
        vector<Polygon> Polygons;
        BoundingBox BoundingBox;
    };




}

#endif
