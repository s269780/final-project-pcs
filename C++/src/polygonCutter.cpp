#include "polygonCutter.hpp"

namespace polygonCutter {

OutputProblemData ConvexPolygonCutter::SplitPolygon() const
{
    //Caso convesso: solo poligoni dx e sx
    vector<Vertex> leftSidePoly, rightSidePoly;

    const vector<Vertex>& startingPolygon = _problemData.Polygon;
    const Segment& splitLine = _problemData.SplitLine;

    int numOfVertices = startingPolygon.size();

    //Make sure there's enough space in the vector, the number of vertices can at most increase by 4
    leftSidePoly.reserve(numOfVertices+4);
    rightSidePoly.reserve(numOfVertices+4);

    //elenco dei vertici
    vector<Vertex> allVertices = startingPolygon;
    allVertices.reserve(numOfVertices+4);
    //aggiungo i vertici del segmento all'elenco generale
    allVertices.push_back(splitLine.First);
    allVertices.push_back(splitLine.Second);

    //Prendo primo vertice
    Vertex current = startingPolygon[0];
    SideOfVertex currentSide = GetVertexSide(splitLine, current);

    for(int v=1; v<=numOfVertices; v++) {
        Vertex next = startingPolygon[v%numOfVertices];
        SideOfVertex nextSide = GetVertexSide(splitLine, next);

        if(currentSide != SideOfVertex::OnLine) {
            if(currentSide==SideOfVertex::Right)
                rightSidePoly.push_back(current);
            else
                leftSidePoly.push_back(current);
            if(nextSide!=SideOfVertex::OnLine && currentSide!=nextSide) {
                const Vertex& intersection = ComputeIntersection(splitLine, Segment(current,next), allVertices.size());
                rightSidePoly.push_back(intersection);
                leftSidePoly.push_back(intersection);
                allVertices.push_back(intersection);
            }
        } else {
            rightSidePoly.push_back(current);
            leftSidePoly.push_back(current);
        }

        //Aggiorna current con il precedente next
        current = next;
        currentSide = nextSide;

    }

    OutputProblemData output;
    output.Vertices = allVertices;
    output.Polygons.reserve(2);

    //Aggiunge gli eventuali vertici della splitLine

    int leftSize = leftSidePoly.size();
    int rightSize = rightSidePoly.size();

    if(leftSize>2) {
        AddSplitLineVerticesToPolygon(leftSidePoly,splitLine);
        output.Polygons.push_back(leftSidePoly);
    }

    if(rightSize>2) {
        AddSplitLineVerticesToPolygon(rightSidePoly,splitLine);
        output.Polygons.push_back(rightSidePoly);
    }

    return output;
}

SideOfVertex AbstractPolygonCutter::GetVertexSide(const Segment &segment, const Vertex &point)
{
    MatrixXd vectorProduct(2,2);
    vectorProduct(0,0) = segment.Second.X - segment.First.X;
    vectorProduct(0,1) = segment.Second.Y - segment.First.Y;
    vectorProduct(1,0) = point.X - segment.First.X;
    vectorProduct(1,1) = point.Y - segment.First.Y;

    double det = vectorProduct.determinant();
    double normProduct = 1. / (vectorProduct.row(0).norm()*vectorProduct.row(1).norm());
    double angle = det*normProduct;

    if(angle>TOLERANCE_PARALLELISM)
        return SideOfVertex::Left;
    else if(angle<-TOLERANCE_PARALLELISM)
        return SideOfVertex::Right;
    else
        return SideOfVertex::OnLine;
}

Vertex AbstractPolygonCutter::ComputeIntersection(const Segment &splitLine, const Segment &edge, int nextId) const
{
    Matrix2d tangentVectors;
    tangentVectors(0,0) = splitLine.Second.X - splitLine.First.X;
    tangentVectors(1,0) = splitLine.Second.Y - splitLine.First.Y;
    tangentVectors(0,1) = edge.First.X - edge.Second.X;
    tangentVectors(1,1) = edge.First.Y - edge.Second.Y;
    double det = tangentVectors.determinant();

    Vector2d rightHandSide;
    rightHandSide(0) = edge.First.X - splitLine.First.X;
    rightHandSide(1) = edge.First.Y - splitLine.First.Y;

    //We've already checked there is no parallelism
    //Solve the system

    Vector2d resultParametricCoordinates;
    Matrix2d solverMatrix;
    solverMatrix << tangentVectors(1,1), -tangentVectors(0,1), -tangentVectors(1,0), tangentVectors(0,0);
    resultParametricCoordinates = solverMatrix * rightHandSide;
    resultParametricCoordinates /= det;

    double intersectionX = edge.First.X - resultParametricCoordinates(1)*tangentVectors(0,1);
    double intersectionY = edge.First.Y - resultParametricCoordinates(1)*tangentVectors(1,1);

    return Vertex(intersectionX,intersectionY, nextId);
}

double AbstractPolygonCutter::GetParametricCoordinate(const Segment &segment, const Vertex &point)
{
    Vector2d segmentVector(segment.Second.X-segment.First.X, segment.Second.Y-segment.First.Y);
    Vector2d pointVector(point.X-segment.First.X, point.Y-segment.First.Y);

    double invSquaredNorm = 1./segmentVector.squaredNorm();
    double parCoord = segmentVector.dot(pointVector)*invSquaredNorm;

    return parCoord;
}

void AbstractPolygonCutter::AddSplitLineVerticesToPolygon(vector<Vertex> &polygon, const Segment &splitLine) const
{
    int size = polygon.size();
    for(int e = 0; e < size; e++) {
        bool isFirstOnEdge = false;
        bool isSecondOnEdge = false;
        double firstParCoord;
        double secondParCoord;
        Segment currentEdge(polygon[e], polygon[(e+1)%size]);
        if(GetVertexSide(currentEdge, splitLine.First) == SideOfVertex::OnLine) {
            firstParCoord = GetParametricCoordinate(currentEdge, splitLine.First);
            if((firstParCoord > TOLERANCE_DISTANCE) && (firstParCoord < (1.-TOLERANCE_DISTANCE)))
                isFirstOnEdge = true;
        }
        if(GetVertexSide(currentEdge, splitLine.Second) == SideOfVertex::OnLine) {
            secondParCoord = GetParametricCoordinate(currentEdge, splitLine.Second);
            if(secondParCoord > TOLERANCE_DISTANCE && secondParCoord < (1. - TOLERANCE_DISTANCE))
                isSecondOnEdge = true;
        }

        if(isFirstOnEdge && isSecondOnEdge) {
            if(secondParCoord > firstParCoord) {
                vector<Vertex>::iterator it = polygon.begin()+e+1;
                it = polygon.insert(it, splitLine.First)+1;
                polygon.insert(it, splitLine.Second);
                break;
            }
            else {
                vector<Vertex>::iterator it = polygon.begin()+e+1;
                it = polygon.insert(it, splitLine.Second)+1;
                polygon.insert(it, splitLine.First);
                break;
            }
        } else if(isFirstOnEdge) {
            vector<Vertex>::iterator it = polygon.begin()+e+1;
            polygon.insert(it, splitLine.First);
            e++;
            size++;
        } else if(isSecondOnEdge) {
            vector<Vertex>::iterator it = polygon.begin()+e+1;
            polygon.insert(it, splitLine.Second);
            e++;
            size++;
        }
    }
}


OutputProblemData ConcavePolygonCutter::SplitPolygon() const
{
    const vector<Vertex>& startingPolygon = _problemData.Polygon;
    const Segment& splitLine = _problemData.SplitLine;

    int numOfVertices = startingPolygon.size();

    vector<VertexPointer> splitPoly;

    //elenco dei vertici
    vector<Vertex> allVertices;
    vector<int> verticesOnLine;

    allVertices = startingPolygon;
    allVertices.push_back(splitLine.First);
    allVertices.push_back(splitLine.Second);

    Vertex current = startingPolygon[0];
    SideOfVertex currentSide = GetVertexSide(splitLine, current);
    // iterazione sui vertici del poligono
    for (int i=1; i<=numOfVertices; i++){
        Vertex next = startingPolygon[i%numOfVertices];
        SideOfVertex nextSide = GetVertexSide(splitLine, next);

        splitPoly.push_back(VertexPointer(i-1));

        // .back() ritorna una referenza all'ultimo elemento dell'array
        if (currentSide == SideOfVertex::OnLine)
            verticesOnLine.push_back(splitPoly.size()-1);
        else if (currentSide != nextSide && nextSide != SideOfVertex::OnLine)
        {
            Vertex intersection = ComputeIntersection(splitLine, Segment(current,next), allVertices.size());
            splitPoly.push_back(VertexPointer(intersection.Id));
            verticesOnLine.push_back(splitPoly.size()-1); // perchè non verticesOnLine.push_back(&splitPoly.back()); ?? tanto hai appena aggiunto intersection a splitPoly
            allVertices.push_back(intersection);
        }
        current=next;
        currentSide=nextSide;
    }

    //Connetto tutti i riferimenti Next e Prev
    int splitPolySize = splitPoly.size();
    for (int i = 0; i<splitPolySize; i++)
    {
        splitPoly[i].PrevPos=((i+splitPolySize-1)%splitPolySize); //per risolvere il problema del -1
        splitPoly[i].NextPos=((i+1)%splitPolySize);
    }

    //Calcolo la coordinata parametrica di ogni punto sulla splitLine
    for (unsigned int i = 0; i<verticesOnLine.size(); i++)
    {
        int currentPos = verticesOnLine[i];
        splitPoly[currentPos].ParCoordOnLine = GetParametricCoordinate(splitLine,allVertices[splitPoly[currentPos].Id]);
    }

    //Ordino la lista dei vertici sulla splitLine
    sort(verticesOnLine.begin(),verticesOnLine.end(),[splitPoly](int x, int y){return splitPoly[x].ParCoordOnLine < splitPoly[y].ParCoordOnLine;});

    //Identifica i punti sorgente e destinazione e crea i lati comuni
    bool alreadyFoundSource = false;
    int sourcePos;
    for (unsigned int i = 0; i<verticesOnLine.size(); i++) {

        //Trova la sorgente
        VertexPointer currPtr;
        bool foundSource = alreadyFoundSource;
        alreadyFoundSource = false;
        int destinationPos;
        while(!foundSource && i<verticesOnLine.size()) {
            currPtr = splitPoly[verticesOnLine[i]];
            SideOfVertex prevSide = GetVertexSide(splitLine,allVertices[splitPoly[currPtr.PrevPos].Id]);
            SideOfVertex nextSide = GetVertexSide(splitLine,allVertices[splitPoly[currPtr.NextPos].Id]);

            if ((prevSide == SideOfVertex::Left && nextSide == SideOfVertex::Right) ||
               (prevSide == SideOfVertex::Left && nextSide == SideOfVertex::OnLine && splitPoly[currPtr.NextPos].ParCoordOnLine < currPtr.ParCoordOnLine) ||
               (prevSide == SideOfVertex::OnLine && nextSide == SideOfVertex::Right && splitPoly[currPtr.PrevPos].ParCoordOnLine < currPtr.ParCoordOnLine)) {
               sourcePos = verticesOnLine[i];
               foundSource = true;
            }
            else
               i++;
        }

        //Trova la destinazione
        bool foundDst = false;
        while(!foundDst && i<verticesOnLine.size())
        {
           currPtr = splitPoly[verticesOnLine[i]];
           SideOfVertex prevSide = GetVertexSide(splitLine,allVertices[splitPoly[currPtr.PrevPos].Id]);
           SideOfVertex nextSide = GetVertexSide(splitLine,allVertices[splitPoly[currPtr.NextPos].Id]);

           if ((prevSide == SideOfVertex::Right && nextSide == SideOfVertex::Left) ||
               (prevSide == SideOfVertex::OnLine && nextSide == SideOfVertex::Left) ||
               (prevSide == SideOfVertex::Right && nextSide == SideOfVertex::OnLine) ||
               (prevSide == SideOfVertex::Right && nextSide == SideOfVertex::Right) ||
               (prevSide == SideOfVertex::Left && nextSide == SideOfVertex::Left)) {
               destinationPos = verticesOnLine[i];
               foundDst = true;
           }
           else
               i++;
        }

        //Raddoppia i lati in comune generandone uno orientato in senso opposto
        if (foundDst && foundSource) {
            VertexPointer a(splitPoly[sourcePos].Id);
            VertexPointer b(splitPoly[destinationPos].Id);
            a.NextPos = destinationPos;
            a.PrevPos = splitPoly[sourcePos].PrevPos;
            b.NextPos = sourcePos;
            b.PrevPos = splitPoly[destinationPos].PrevPos;
            splitPoly.push_back(a);
            splitPoly.push_back(b);
            splitPoly[splitPoly[sourcePos].PrevPos].NextPos = splitPoly.size()-2;
            splitPoly[sourcePos].PrevPos = splitPoly.size()-1;
            splitPoly[splitPoly[destinationPos].PrevPos].NextPos = splitPoly.size()-1;
            splitPoly[destinationPos].PrevPos = splitPoly.size()-2;


            // Controlla se la destinazione è una nuova sorgente
            if (GetVertexSide(splitLine, allVertices[splitPoly[splitPoly[splitPoly[sourcePos].PrevPos].PrevPos].Id]) == SideOfVertex::Left) {
                sourcePos = splitPoly[sourcePos].PrevPos;
                alreadyFoundSource = true;
            } else if (GetVertexSide(splitLine, allVertices[splitPoly[splitPoly[destinationPos].NextPos].Id]) == SideOfVertex::Right) {
                sourcePos = destinationPos;
                alreadyFoundSource = true;
            }
        }
    }

    OutputProblemData output;

    for (unsigned int i = 0; i<splitPoly.size(); i++)
    {
        if (!splitPoly[i].Visited)
        {
            vector<Vertex> newPoly;
            unsigned int currPos = i;

            do
            {
                splitPoly[currPos].Visited = true;
                newPoly.push_back(allVertices[splitPoly[currPos].Id]);
                currPos=splitPoly[currPos].NextPos;
            }
            while (currPos != i);

            //Aggiungo gli eventuali vertici della splitLine
            AddSplitLineVerticesToPolygon(newPoly, splitLine);

            output.Polygons.push_back(newPoly);
        }
    }

    for (unsigned int i = 0; i<allVertices.size(); i++) {
        output.Vertices.push_back(allVertices[i]);
    }
    return output;
}

}
