#ifndef POLYGONCUTTER_H
#define POLYGONCUTTER_H

#include "abstractClassesAndDTO.hpp"
#include <Eigen>
#include <iostream>
#include <list>

using namespace Eigen;
using namespace std;
using namespace abstractClassesAndDTO;

namespace polygonCutter {

    class AbstractPolygonCutter {
    public:
        static SideOfVertex GetVertexSide(const Segment& segment, const Vertex& point);
        Vertex ComputeIntersection(const Segment& splitLine, const Segment& edge, int nextId) const;
        static double GetParametricCoordinate(const Segment& segment, const Vertex& point);
        void AddSplitLineVerticesToPolygon(vector<Vertex>& polygon, const Segment& splitLine) const;
        virtual OutputProblemData SplitPolygon() const = 0;

    };

    class ConvexPolygonCutter: public AbstractPolygonCutter {
    private:
        InputProblemData& _problemData;
    public:
        ConvexPolygonCutter(InputProblemData& problemData) : _problemData(problemData) { };
        OutputProblemData SplitPolygon() const ;
    };

    class ConcavePolygonCutter: public AbstractPolygonCutter {
    private:
        InputProblemData& _problemData;
    public:
        ConcavePolygonCutter(InputProblemData& problemData) : _problemData(problemData) { };
        OutputProblemData SplitPolygon() const;
    };


}

#endif // POLYGONCUTTER_H
