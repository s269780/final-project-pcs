#include "problemFunctions.hpp"

namespace problemFunctions {

OutputProblemData ProblemFunctions::CutPolygon(vector<Vertex> inputVertices, vector<int> inputPolygon, vector<Vertex> inputSplitLine)
{
    //Convert input into ProblemData DTO

    InputProblemData inputData;

    inputData.SplitLine = Segment(inputSplitLine[0], inputSplitLine[1]);
    int polySize = inputPolygon.size();
    inputData.Polygon = vector<Vertex>(polySize);
    for(int i = 0; i < polySize; i++) {
        inputData.Polygon[i] = inputVertices[inputPolygon[i]];
    }


    // Distinguish whether the polygon is convex or concave
    bool isConcave = false;
    for(int i = 0; i < polySize; i++) {
        if(AbstractPolygonCutter::GetVertexSide(Segment(inputData.Polygon[i],inputData.Polygon[(i+1)%polySize]),
                                                inputData.Polygon[(i+2)%polySize]) == SideOfVertex::Right) {
            isConcave = true;
            break;
        }
    }

    if (isConcave) {
        ConcavePolygonCutter cutter(inputData);
        return cutter.SplitPolygon();
    } else {
        ConvexPolygonCutter cutter(inputData);
        return cutter.SplitPolygon();
    }
}

ReferenceElement ProblemFunctions::ComputeReferenceElement(const Polygon& poly)
{
    BoundingBox boundingBox = poly.ComputeBoundingBox();
    double height = boundingBox.NE.Y-boundingBox.SW.Y;
    double base = boundingBox.NE.X-boundingBox.SW.X;
    Vertex east, west, north, south;
    int size = poly.Vertices.size();
    for(int i = 0; i < size; i++) {
        if(abs(poly.Vertices[i].X-boundingBox.NE.X)<TOLERANCE_DISTANCE)
            east = poly.Vertices[i];
        if(abs(poly.Vertices[i].X-boundingBox.SW.X)<TOLERANCE_DISTANCE)
            west = poly.Vertices[i];
        if(abs(poly.Vertices[i].Y-boundingBox.NE.Y)<TOLERANCE_DISTANCE)
            north = poly.Vertices[i];
        if(abs(poly.Vertices[i].Y-boundingBox.SW.Y)<TOLERANCE_DISTANCE)
            south = poly.Vertices[i];
    }

    Vertex repNorth=north;
    repNorth.Y-=height;
    Vertex repSouth=south;
    repSouth.Y+=height;
    Vertex repEast=east;
    repEast.X-=base;
    Vertex repWest=west;
    repWest.X+=base;
    vector<Vertex> pointsToMakeConformalMesh = {repEast,repNorth,repSouth,repWest};

    vector<Vertex> northEastVertices;
    northEastVertices.push_back(boundingBox.NE);
    northEastVertices.push_back(north);
    int pos=0;
    while(!(poly.Vertices[pos]==north))
        pos++;
    pos=(pos+size-1)%size;
    while(!(poly.Vertices[pos]==east)) {
        northEastVertices.push_back(poly.Vertices[pos]);
        pos=(pos-1)%size;
    }
    northEastVertices.push_back(east);

    vector<Vertex> northWestVertices;
    northWestVertices.push_back(boundingBox.NW);
    northWestVertices.push_back(west);
    pos=0;
    while(!(poly.Vertices[pos]==west))
        pos++;
    pos=(pos+size-1)%size;
    while(!(poly.Vertices[pos]==north)) {
        northWestVertices.push_back(poly.Vertices[pos]);
        pos=(pos-1)%size;
    }
    northWestVertices.push_back(north);

    vector<Vertex> southWestVertices;
    southWestVertices.push_back(boundingBox.SW);
    southWestVertices.push_back(south);
    pos=0;
    while(!(poly.Vertices[pos]==south))
        pos++;
    pos=(pos+size-1)%size;
    while(!(poly.Vertices[pos]==west)) {
        southWestVertices.push_back(poly.Vertices[pos]);
        pos=(pos-1)%size;
    }
    southWestVertices.push_back(west);

    vector<Vertex> southEastVertices;
    southEastVertices.push_back(boundingBox.SE);
    southEastVertices.push_back(east);
    pos=0;
    while(!(poly.Vertices[pos]==east))
        pos++;
    pos=(pos+size-1)%size;
    while(!(poly.Vertices[pos]==south)) {
        southEastVertices.push_back(poly.Vertices[pos]);
        pos=(pos-1)%size;
    }
    southEastVertices.push_back(south);
    Polygon northEastPoly(northEastVertices);
    Polygon northWestPoly(northWestVertices);
    Polygon southWestPoly(southWestVertices);
    Polygon southEastPoly(southEastVertices);
    northEastPoly.AddIntermediatePoints(pointsToMakeConformalMesh);
    northWestPoly.AddIntermediatePoints(pointsToMakeConformalMesh);
    southWestPoly.AddIntermediatePoints(pointsToMakeConformalMesh);
    southEastPoly.AddIntermediatePoints(pointsToMakeConformalMesh);

    ReferenceElement output;
    output.Polygons = {poly,northEastPoly,northWestPoly,southWestPoly,southEastPoly};
    output.BoundingBox = boundingBox;

    return output;
}

vector<Polygon> ProblemFunctions::CreateMesh(const Polygon& domain, const ReferenceElement& refElem) {
    double lenDomain, heightDomain, lenRefEl, heightRefEl;
    lenDomain = domain.Vertices[1].X - domain.Vertices[0].X;
    heightDomain = domain.Vertices[2].Y - domain.Vertices[1].Y;
    lenRefEl = refElem.BoundingBox.SE.X - refElem.BoundingBox.SW.X;
    heightRefEl = refElem.BoundingBox.NE.Y - refElem.BoundingBox.SE.Y;
    int n = floor(lenDomain / lenRefEl);
    int m = floor(heightDomain / heightRefEl);

    vector<Polygon> meshPolygons;
    vector<Polygon> alreadyCutRightRefElem;

    for(int i = 0; i < n; i++){
        for(int j = 0; j < m; j++){
            //itero sui poligoni dell'elemento di riferimento
            for(unsigned int p = 0; p < refElem.Polygons.size(); p++){
                // itero sui vertici del poligono considerato
                Polygon newPoly = refElem.Polygons[p];
                newPoly.TraslateVertices(i*lenRefEl,j*heightRefEl);
                meshPolygons.push_back(newPoly);
            }
        }
    }
    double nFloat = lenDomain / lenRefEl;
    double mFloat = heightDomain / heightRefEl;
    //mettere la tolleranza
    //if (nFloat > n && mFloat > m){
    if(nFloat > n+TOLERANCE_DISTANCE){

        Segment segmentVertical;
        segmentVertical.First.X = refElem.BoundingBox.SW.X + (nFloat - n) * lenRefEl;
        segmentVertical.First.Y = refElem.BoundingBox.SW.Y;
        segmentVertical.Second.X = refElem.BoundingBox.NW.X + (nFloat - n) * lenRefEl;
        segmentVertical.Second.Y = refElem.BoundingBox.NW.Y;

        //aggiungere poligoni dell'elem di riferimento parzialmente tagliato a destra
        for (int i = 0; i < refElem.Polygons.size(); i++){

            vector<int> idsPolygon;
            for(int v = 0; v < refElem.Polygons[i].Vertices.size(); v++){
                idsPolygon.push_back(v);

            }


            OutputProblemData output;
            output = ProblemFunctions::CutPolygon(refElem.Polygons[i].Vertices, idsPolygon,{segmentVertical.First, segmentVertical.Second});


            // output contiene i poligoni generati dal taglio
            //aggiungo i poligoni a sinistra della figura intera

            for (int p = 0; p < output.Polygons.size(); p++){
                bool flag = false;
                for(int k = 0; k < output.Polygons[p].size(); k++){
                    if(AbstractPolygonCutter::GetVertexSide(segmentVertical,output.Polygons[p][k]) == SideOfVertex::Left){
                        flag = true;
                        break;
                    }
                }


                if (flag == true){
                    alreadyCutRightRefElem.push_back(output.Polygons[p]);
                    for(int j = 0; j < m; j++){
                        Polygon newPoly;
                        newPoly.Vertices = output.Polygons[p];
                        newPoly.TraslateVertices(n*lenRefEl, j*heightRefEl);
                        meshPolygons.push_back(newPoly);
                    }

                }

            }


         }
    }

    Segment segmentHorizontal;
    segmentHorizontal.First.X = refElem.BoundingBox.SW.X;
    segmentHorizontal.First.Y = refElem.BoundingBox.SW.Y + (mFloat - m) * heightRefEl;
    segmentHorizontal.Second.X = refElem.BoundingBox.SE.X;
    segmentHorizontal.Second.Y = refElem.BoundingBox.SE.Y + (mFloat - m) * heightRefEl;
    if (mFloat > m+TOLERANCE_DISTANCE){
        //aggiungere poligoni dell'elem di riferimento parzialmente tagliato in alto


        for (int i = 0; i < refElem.Polygons.size(); i++){

            vector<int> idsPolygon;
            for(int v = 0; v < refElem.Polygons[i].Vertices.size(); v++){
                idsPolygon.push_back(v);

            }


            OutputProblemData output;
            output = ProblemFunctions::CutPolygon(refElem.Polygons[i].Vertices, idsPolygon,{segmentHorizontal.First, segmentHorizontal.Second});


            // output contiene i poligoni generati dal taglio
            //aggiungo i poligoni a sinistra della figura intera

            for (int p = 0; p < output.Polygons.size(); p++){
                bool flag = false;
                for(int k = 0; k < output.Polygons[p].size(); k++){
                    if(AbstractPolygonCutter::GetVertexSide(segmentHorizontal,output.Polygons[p][k]) == SideOfVertex::Right){
                        flag = true;
                        break;
                    }
                }

                if (flag == true){
                    for(int j = 0; j < n; j++){
                        Polygon newPoly;
                        newPoly.Vertices = output.Polygons[p];
                        newPoly.TraslateVertices(j*lenRefEl, m*heightRefEl);
                        meshPolygons.push_back(newPoly);
                    }

                }
            }
        }
    }
    if (nFloat > n+TOLERANCE_DISTANCE && mFloat > m+TOLERANCE_DISTANCE){
        //aggiungere il poligono in alto a destra

        for (int i = 0; i < alreadyCutRightRefElem.size(); i++){

            vector<int> idsPolygon;
            for(int v = 0; v < alreadyCutRightRefElem[i].Vertices.size(); v++){
                idsPolygon.push_back(v);
            }

            OutputProblemData output;
            output = ProblemFunctions::CutPolygon(alreadyCutRightRefElem[i].Vertices, idsPolygon,{segmentHorizontal.First, segmentHorizontal.Second});

            for (int p = 0; p < output.Polygons.size(); p++){
                bool flag = false;
                for(int k = 0; k < output.Polygons[p].size(); k++){
                    if(AbstractPolygonCutter::GetVertexSide(segmentHorizontal,output.Polygons[p][k]) == SideOfVertex::Right){
                        flag = true;
                        break;
                    }
                }


                if (flag == true){
                    Polygon newPoly;
                    newPoly.Vertices = output.Polygons[p];
                    newPoly.TraslateVertices(n*lenRefEl, m*heightRefEl);
                    meshPolygons.push_back(newPoly);
                    }

            }
        }

    }

    return meshPolygons;
}


}

