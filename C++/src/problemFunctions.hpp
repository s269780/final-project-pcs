#ifndef PROBLEMFUNCTIONS_H
#define PROBLEMFUNCTIONS_H

#include "polygonCutter.hpp"
#include "mesh.hpp"

using namespace std;
using namespace polygonCutter;
using namespace mesh;

namespace problemFunctions {

    class ProblemFunctions {
    public:
        static OutputProblemData CutPolygon(vector<Vertex> inputVertices, vector<int> inputPolygon, vector<Vertex> splitLine);
        static ReferenceElement ComputeReferenceElement(const Polygon& poly);
        static vector<Polygon> CreateMesh(const Polygon& domain, const ReferenceElement& refElem);
};
}

#endif
