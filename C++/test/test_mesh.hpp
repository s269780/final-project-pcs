#ifndef __TEST_MESH_H
#define __TEST_MESH_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "problemFunctions.hpp"

using namespace problemFunctions;
using namespace testing;
using namespace std;

namespace meshTesting {
TEST(TestMesh, TestComputeArea) {

    vector<Vertex> vertices = {Vertex(0,0,1),Vertex(4,0,2),Vertex(2,2,3),Vertex(4,4,4),Vertex(0,4,5)};
    Polygon poly(vertices);

    ASSERT_FLOAT_EQ(12,poly.ComputeArea());
}

TEST(TestMesh, TestComputeBoundingBox) {

    vector<Vertex> vertices = {Vertex(1,0),Vertex(0,1),Vertex(-1,0),Vertex(-1,-1)};
    Polygon poly(vertices);

    auto out = poly.ComputeBoundingBox();
    Vertex NE(1,1);
    Vertex NW(-1,1);
    Vertex SW(-1,-1);
    Vertex SE(1,-1);
    ASSERT_EQ(out.NE, NE);
    ASSERT_EQ(out.NW, NW);
    ASSERT_EQ(out.SE, SE);
    ASSERT_EQ(out.SW, SW);
}

TEST(TestMesh, TestTraslateVertices) {
    vector<Vertex> vertices = {Vertex(1,0),Vertex(0,1),Vertex(-1,0),Vertex(-1,-1)};
    Polygon poly(vertices);

    poly.TraslateVertices(0.5,1.5);
    Vertex V0(1.5,1.5);
    Vertex V1(0.5,2.5);
    Vertex V2(-0.5,1.5);
    Vertex V3(-0.5,0.5);
    ASSERT_EQ(poly.Vertices[0], V0);
    ASSERT_EQ(poly.Vertices[1], V1);
    ASSERT_EQ(poly.Vertices[2], V2);
    ASSERT_EQ(poly.Vertices[3], V3);
}

TEST(TestMesh, TestCreateReferenceElement) {

    Vertex P0(1.5,0,0);
    Vertex P1(2,0.4,1);
    Vertex P2(1.3,1,2);
    Vertex P3(1,0.5,3);
    Vertex P4(0,0.3,4);
    Polygon poly ({P0,P1,P2,P3,P4});

    auto out = ProblemFunctions::ComputeReferenceElement(poly);

    double area = 0;

    for(unsigned int i = 0; i < out.Polygons.size(); i++) {
        area+=out.Polygons[i].ComputeArea();
    }

    EXPECT_FLOAT_EQ(area,2);
}

TEST(TestMesh, TestAddIntermediatePoints) {
    vector<Vertex> vertices = {Vertex(1,0),Vertex(0,1),Vertex(-1,0),Vertex(-1,-1)};
    Polygon poly(vertices);

    Vertex P(0.5,0.5);
    poly.AddIntermediatePoints({P});

    ASSERT_EQ(poly.Vertices.size(),5);
    ASSERT_EQ(poly.Vertices[1], P);
}

TEST(TestMesh, TestCreateMesh) {

    vector<Vertex> Polygon1 = {Vertex(1,0,5),Vertex(2,0.4,6),Vertex(1,1,7),Vertex(0.5,0.8,8),Vertex(0,0.4,9)};
    vector<Vertex> Polygon2 = {Vertex(0,0,1),Vertex(1,0,5),Vertex(0,0.4,9)};
    vector<Vertex> Polygon3 = {Vertex(1,0,5),Vertex(2,0,2),Vertex(2,0.4,6)};
    vector<Vertex> Polygon4 = {Vertex(2,0.4,6),Vertex(2,1,3),Vertex(1,1,7)};
    vector<Vertex> Polygon5 = {Vertex(1,1,7),Vertex(0,1,4),Vertex(0,0.4,9),Vertex(0.5,0.8,8)};
    BoundingBox BB;
    BB.SW = Vertex(0,0,1);
    BB.SE = Vertex(2,0,2);
    BB.NE = Vertex(2,1,3);
    BB.NW = Vertex(0,1,4);
    vector<Polygon> polyRefElem = {Polygon1, Polygon2, Polygon3, Polygon4, Polygon5};
    ReferenceElement refEl;
    refEl.BoundingBox = BB;
    refEl.Polygons = polyRefElem;

    vector<Vertex> domain = {Vertex(0,0,1),Vertex(5,0,10),Vertex(5,2.8,11),Vertex(0,2.8,12)};

    vector<Polygon> output = ProblemFunctions::CreateMesh(domain, refEl);

    double area = 0;

    for(unsigned int i = 0; i < output.size(); i++) {
        area+=output[i].ComputeArea();
    }

    EXPECT_FLOAT_EQ(area,14);

}
TEST(TestMesh, TestFinal) {
    Vertex P0(1.5,0,0);
    Vertex P1(2,0.4,1);
    Vertex P2(1.3,1.2,2);
    Vertex P3(1,0.5,3);
    Vertex P4(0,0.3,4);
    Polygon poly ({P0,P1,P2,P3,P4});

    auto refEl = ProblemFunctions::ComputeReferenceElement(poly);

    double length = refEl.BoundingBox.NE.X-refEl.BoundingBox.NW.X;
    double height = refEl.BoundingBox.NE.Y-refEl.BoundingBox.SE.Y;

    Polygon domain({Vertex(0,0),Vertex(4*length+1,0),Vertex(4*length+1,3*height+1),Vertex(0,3*height+1)});
    auto out = ProblemFunctions::CreateMesh(domain,refEl);

    double area = 0;

    for(unsigned int i = 0; i < out.size(); i++) {
        area+=out[i].ComputeArea();
    }

    EXPECT_FLOAT_EQ(area,(4*length+1)*(3*height+1));
}

}

#endif
