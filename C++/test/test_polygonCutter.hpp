#ifndef __TEST_POLYGONCUTTER_H
#define __TEST_POLYGONCUTTER_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "problemFunctions.hpp"

using namespace problemFunctions;
using namespace testing;
using namespace std;

namespace polygonCutterTesting {

TEST(TestCutPolygon, TestConcave1) {

    Vertex P0(1.5,1.0,0);
    Vertex P1(5.6,1.5,1);
    Vertex P2(5.5,4.8,2);
    Vertex P3(4.0,6.2,3);
    Vertex P4(3.2,4.2,4);
    Vertex P5(1.0,4.0,5);
    Vertex S1(2.0,3.7,6);
    Vertex S2(4.1,5.9,7);

    OutputProblemData expected;
    Vertex P8(4.2043269230,6.009294871,8);
    Vertex P9(3.7213114754098,5.5032786885246,9);
    Vertex P10(2.4085972850679,4.1280542986425,10);
    Vertex P11(1.1912162162162,2.8527027027027,11);

    expected.Vertices = {P0,P1,P2,P3,P4,P5,S1,S2,P8,P9,P10,P11};
    expected.Polygons = {{P0,P1,P2,P8,S2,P9,P4,P10,S1,P11},{P8,P3,P9,S2},{P10,P5,P11,S1}};

    auto out = ProblemFunctions::CutPolygon({P0,P1,P2,P3,P4,P5},{0,1,2,3,4,5},{S1,S2});
    ASSERT_EQ(out,expected);
}

TEST(TestCutPolygon, TestConcave2) {

    Vertex P0(2,2,0);
    Vertex P1(5,2,1);
    Vertex P2(6,3,2);
    Vertex P3(5,6,3);
    Vertex P4(4,5,4);
    Vertex P5(3,6,5);
    Vertex P6(1,3,6);
    Vertex S2(3,5,8);
    Vertex S1(5,5,7);

    OutputProblemData expected;
    Vertex P9(5.333333333,5,9);
    Vertex P10(2.3333333333,5,10);

    expected.Vertices = {P0,P1,P2,P3,P4,P5,P6,S1,S2,P9,P10};
    expected.Polygons = {{P0,P1,P2,P9,S1,P4,S2,P10,P6},{P9,P3,P4,S1},{P4,P5,P10,S2}};

    auto out = ProblemFunctions::CutPolygon({P0,P1,P2,P3,P4,P5,P6},{0,1,2,3,4,5,6},{S1,S2});
    ASSERT_EQ(out,expected);
}

TEST(TestCutPolygon, TestRectangle) {

    Vertex P0(1,1,0);
    Vertex P1(5,1,1);
    Vertex P2(5,3.1,2);
    Vertex P3(1,3.1,3);
    Vertex S1(2.0,1.2,4);
    Vertex S2(4,3,5);

    OutputProblemData expected;
    Vertex P6(1.77777777,1,6);
    Vertex P7(4.11111111,3.1,7);

    expected.Polygons = {{P0,P6,S1,S2,P7,P3},{P6,P1,P2,P7,S2,S1}};
    expected.Vertices = {P0,P1,P2,P3,S1,S2,P6,P7};

    auto out2 = ProblemFunctions::CutPolygon({P0,P1,P2,P3},{0,1,2,3},{S1,S2});
    ASSERT_EQ(out2,expected);
}

TEST(TestCutPolygon, TestPentagon) {

    Vertex P0(2.5,1.1,0);
    Vertex P1(4,2.1,1);
    Vertex P2(3.4,4.2,2);
    Vertex P3(1.6,4.2,3);
    Vertex P4(1,2.1,4);
    Vertex S1(1.4,2.75,5);
    Vertex S2(3.6,2.2,6);

    OutputProblemData expected;
    Vertex P7(1.2,2.8,7);

    expected.Vertices = {P0,P1,P2,P3,P4,S1,S2,P7};
    expected.Polygons = {{P1,P2,P3,P7,S1,S2},{P0,P1,S2,S1,P7,P4},};

    auto out = ProblemFunctions::CutPolygon({P0,P1,P2,P3,P4},{0,1,2,3,4},{S1,S2});
    ASSERT_EQ(out,expected);
}

TEST(TestCutPolygon, TestConvex) {

    Vertex S1(4.0, 3.0 ,6);
    Vertex S2(7.0, 6.0, 7);
    Vertex P0(1.0, 1.0, 0);
    Vertex P1(7.0, 1.0, 1);
    Vertex P2(8.0, 3.0, 2);
    Vertex P3(6.0, 5.0, 3);
    Vertex P4(3.0, 8.0, 4);
    Vertex P5(0.0, 4.0, 5);
    Vertex P8(2.0, 1.0, 8);

    OutputProblemData expected;

    expected.Vertices = {P0,P1,P2,P3,P4,P5,S1,S2,P8};
    expected.Polygons = {{P0,P8,S1,P3,P4,P5},{P8,P1,P2,P3,S1}};

    auto out = ProblemFunctions::CutPolygon({P0,P1,P2,P3,P4,P5},{0,1,2,3,4,5},{S1,S2});
    ASSERT_EQ(out,expected);
}

TEST(TestPublicMethods, TestGetVertexSide) {

    Vertex S1(0,0,1);
    Vertex S2(1,1,2);
    Vertex Left(0,1,2);
    Vertex Right(1,0,3);
    Vertex OnLine(0.5,0.5,4);

    ASSERT_EQ(AbstractPolygonCutter::GetVertexSide(Segment(S1,S2),Left),SideOfVertex::Left);
    ASSERT_EQ(AbstractPolygonCutter::GetVertexSide(Segment(S1,S2),Right),SideOfVertex::Right);
    ASSERT_EQ(AbstractPolygonCutter::GetVertexSide(Segment(S1,S2),OnLine),SideOfVertex::OnLine);
}

TEST(TestPublicMethods, TestGetParametricCoordinate) {

    InputProblemData input;
    ConvexPolygonCutter cutter(input);
    Vertex S1(0,0,1);
    Vertex S2(1,1,2);
    Vertex V(0.6,0.6,3);
    ASSERT_FLOAT_EQ(cutter.GetParametricCoordinate(Segment(S1,S2),V),0.6);
}

TEST(TestPublicMethods, TestComputeIntersection) {

    InputProblemData input;
    ConvexPolygonCutter cutter(input);
    Vertex S1(0,0,1);
    Vertex S2(1,1,2);
    Vertex S3(1,0,3);
    Vertex S4(0,1,4);
    int nextId = 5;

    ASSERT_EQ(cutter.ComputeIntersection(Segment(S1,S2),Segment(S3,S4),nextId),Vertex(0.5,0.5,5));
}

TEST(TestPublicMethods, TestAddSplitLineVerticesToPolygon) {

    InputProblemData input;
    ConvexPolygonCutter cutter(input);
    Vertex P0(0,0,0);
    Vertex P1(2,0,1);
    Vertex P2(2,2,2);
    Vertex P3(0,2,3);
    Vertex S1(1,0,4);
    Vertex S2(2,1,5);

    vector<Vertex> polygon = {P0,P1,P2,P3};
    Segment segment(S1,S2);

    cutter.AddSplitLineVerticesToPolygon(polygon,segment);

    vector<Vertex> expected = {P0,S1,P1,S2,P2,P3};

    ASSERT_EQ(polygon,expected);
}

TEST(TestPublicMethods, TestSplitPolygonConvex) {

    //Test SplitPolygon in convex case
    Vertex P0(1,1,0);
    Vertex P1(5,1,1);
    Vertex P2(5,3.1,2);
    Vertex P3(1,3.1,3);
    Vertex S1(2.0,1.2,4);
    Vertex S2(4,3,5);

    InputProblemData inputData;

    inputData.SplitLine = Segment(S1,S2);
    inputData.Polygon = {P0,P1,P2,P3};


    OutputProblemData expected;
    Vertex P6(1.77777777,1,6);
    Vertex P7(4.11111111,3.1,7);

    expected.Polygons = {{P0,P6,S1,S2,P7,P3},{P6,P1,P2,P7,S2,S1}};
    expected.Vertices = {P0,P1,P2,P3,S1,S2,P6,P7};

    ConvexPolygonCutter convexCutter(inputData);
    auto output = convexCutter.SplitPolygon();
    ASSERT_EQ(output,expected);

}

TEST(TestPublicMethods, TestSplitPolygonConcave) {

    //Test SplitPoly in concave case

    Vertex P0(1.5,1.0,0);
    Vertex P1(5.6,1.5,1);
    Vertex P2(5.5,4.8,2);
    Vertex P3(4.0,6.2,3);
    Vertex P4(3.2,4.2,4);
    Vertex P5(1.0,4.0,5);
    Vertex S1(2.0,3.7,6);
    Vertex S2(4.1,5.9,7);

    OutputProblemData expected;
    Vertex P8(4.2043269230,6.009294871,8);
    Vertex P9(3.7213114754098,5.5032786885246,9);
    Vertex P10(2.4085972850679,4.1280542986425,10);
    Vertex P11(1.1912162162162,2.8527027027027,11);

    expected.Vertices = {P0,P1,P2,P3,P4,P5,S1,S2,P8,P9,P10,P11};
    expected.Polygons = {{P0,P1,P2,P8,S2,P9,P4,P10,S1,P11},{P8,P3,P9,S2},{P10,P5,P11,S1}};

    InputProblemData inputData;
    inputData.SplitLine = Segment(S1,S2);
    inputData.Polygon = {P0,P1,P2,P3,P4,P5};

    ConcavePolygonCutter cutter(inputData);
    auto output = cutter.SplitPolygon();
    ASSERT_EQ(output, expected);

}

TEST(TestAdvanced, TestAdvanced1) {
    Vertex P0(2,-2,0);
    Vertex P1(0,-1,1);
    Vertex P2(3,1,2);
    Vertex P3(0,2,3);
    Vertex P4(3,2,4);
    Vertex P5(3,3,5);
    Vertex P6(-1,3,6);
    Vertex P7(-3,1,7);
    Vertex P8(0,0,8);
    Vertex P9(-3,-2,9);

    //Uso due vertici esterni alla figura che giacciono sulla retta x=0
    Vertex S1(0,-3,10);
    Vertex S2(0,4,11);

    OutputProblemData expected;

    Vertex P12(0,3,12);
    Vertex P13(0,-2,13);

    expected.Vertices={P0,P1,P2,P3,P4,P5,P6,P7,P8,P9,S1,S2,P12,P13};
    expected.Polygons={{P0,P1,P13},{P1,P2,P3,P8},{P3,P4,P5,P12},{P12,P6,P7,P8,P3},{P8,P9,P13,P1}};

    auto output = ProblemFunctions::CutPolygon({P0,P1,P2,P3,P4,P5,P6,P7,P8,P9},{0,1,2,3,4,5,6,7,8,9},{S1,S2});
    ASSERT_EQ(output,expected);
}

TEST(TestAdvanced, TestAdvanced2) {
    Vertex P0(2,-2,0);
    Vertex P1(0,-1,1);
    Vertex P2(3,1,2);
    Vertex P3(0,2,3);
    Vertex P4(3,2,4);
    Vertex P5(3,3,5);
    Vertex P6(-1,3,6);
    Vertex P7(-3,1,7);
    Vertex P8(0,0,8);
    Vertex P9(-3,-2,9);

    //Uso due vertici esterni alla figura che giacciono sulla retta x=0
    Vertex S1(-4,-4,10);
    Vertex S2(4,4,11);

    OutputProblemData expected;

    Vertex P12(1.5,1.5,12);
    Vertex P13(2,2,13);
    Vertex P14(-2,-2,14);

    expected.Vertices={P0,P1,P2,P3,P4,P5,P6,P7,P8,P9,S1,S2,P12,P13,P14};
    expected.Polygons={{P0,P1,P2,P12,P8,P14},{P12,P3,P13,P5,P6,P7,P8},{P13,P4,P5},{P8,P9,P14}};

    auto output = ProblemFunctions::CutPolygon({P0,P1,P2,P3,P4,P5,P6,P7,P8,P9},{0,1,2,3,4,5,6,7,8,9},{S1,S2});
    ASSERT_EQ(output,expected);
}

}

#endif // __TEST_POLYGONCUTTER_H
