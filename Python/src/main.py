import numpy as np
from numpy import linalg as LA
import math
from enum import Enum
from typing import List

tolParallelism = pow(10, -5)
tolDistance = pow(10, -8)

class SideOfVertex(Enum):
    LEFT = 0
    RIGHT = 1
    ONLINE = 2

class Vertex:
    def __init__(self, x: float, y: float, id: int):
        self.x = x
        self.y = y
        self.id = id

    def __eq__(self,point):
        if self.x + tolDistance >= point.x and self.x - tolDistance <= point.x and self.y + tolDistance >= point.y and self.y - tolDistance <= point.y:
            return True
        else:
            return False

class VertexPointer:
    def __init__(self, id: int):
        self.id = id
        self.prev = -1
        self.next = -1
        self.parCoord = 0.0
        self.Visited = False

class Segment:
    def __init__(self, first: Vertex, second: Vertex):
        self.first = first
        self.second = second

Vertices = List[Vertex]

class InputProblemData:
    def __init__(self, splitLine: Segment, polygon: Vertices):
        self.splitLine = splitLine
        self.polygon = polygon

ListOfPolygons = List[List[Vertex]]
class OutputProblemData:
    def __init__(self):
        self.polygons: ListOfPolygons = []
        self.vertices: Vertices = []

    def __eq__(self,output):
        flag = True
        for i in range(len(self.polygons)):
            for j in range(len(self.polygons[i])):
                if self.polygons[i][j] != output.polygons[i][j]:
                    flag = False
        if flag == True:
            return True
        else:
            return False






class AbstractPolygonCutter:

    # abstract method
    def SplitPolygon(self):
        pass


    @staticmethod
    def GetVertexSide(segment: Segment, point: Vertex):
        vectProd = np.empty((2, 2))
        vectProd[0, 0] = segment.second.x - segment.first.x
        vectProd[0, 1] = segment.second.y - segment.first.y
        vectProd[1, 0] = point.x - segment.first.x
        vectProd[1, 1] = point.y - segment.first.y
        detVectProd = np.linalg.det(vectProd)
        normProduct = 1 / (pow(vectProd[0,0],2) + pow(vectProd[0,1],2))
        angle = detVectProd * normProduct

        if angle > tolParallelism:
            return SideOfVertex.LEFT.value
        elif angle < -tolParallelism:
            return SideOfVertex.RIGHT.value
        else:
            return SideOfVertex.ONLINE.value


    def ComputeIntersection(self, segmentOne: Segment, segmentTwo: Segment, nextId: int):

        tV1 = segmentOne.second.x - segmentOne.first.x
        tV2 = segmentOne.second.y - segmentOne.first.y
        tV3 = segmentTwo.first.x - segmentTwo.second.x
        tV4 = segmentTwo.first.y - segmentTwo.second.y

        tangentVectors = np.matrix([[tV1,tV3], [tV2, tV4]])

        det = LA.det(tangentVectors)
        rHs1 = segmentTwo.first.x - segmentOne.first.x
        rHs2 = segmentTwo.first.y - segmentOne.first.y
        rightHandSide = np.array([[rHs1], [rHs2]])

        solverMatrix = np.matrix([[tV4, -tV3], [-tV2, tV1]])
        resultParametricCoordinates = solverMatrix * rightHandSide
        resultParametricCoordinates = resultParametricCoordinates/det

        x = segmentTwo.first.x - resultParametricCoordinates[1] * tV3
        y = segmentTwo.first.y - resultParametricCoordinates[1] * tV4

        intersectionPoint = Vertex(x, y, nextId)

        return intersectionPoint


    def GetParametricCoordinate(self, segment: Segment, point: Vertex):
        vectorOne = [segment.second.x - segment.first.x, segment.second.y - segment.first.y]
        pointVector = [point.x - segment.first.x, point.y - segment.first.y]
        invSquaredNorm = 1.0 / (vectorOne[0]*vectorOne[0]+vectorOne[1]*vectorOne[1])
        parCoord = (vectorOne[0]*pointVector[0]+vectorOne[1]*pointVector[1]) * invSquaredNorm
        return parCoord


    def AddSplitLineVerticesToPolygon(self, polygon: Vertices, splitLine: Segment):
        # polygon is the current polygon which needs the splitline vertices to be added
        size = len(polygon)
        i = 0
        while i < size:
            isFirstOnEdge = False
            isSecondOnEdge = False
            firstParCoord = 0.0
            secondParCoord = 0.0
            first = polygon[i]
            second = polygon[(i+1) % size]
            currentEdge = Segment(first, second)
            if AbstractPolygonCutter.GetVertexSide(currentEdge, splitLine.first) == SideOfVertex.ONLINE.value:
                firstParCoord = self.GetParametricCoordinate(currentEdge, splitLine.first)
                if firstParCoord > tolDistance and firstParCoord < 1 - tolDistance:
                    isFirstOnEdge = True

            if AbstractPolygonCutter.GetVertexSide(currentEdge, splitLine.second) == SideOfVertex.ONLINE.value:
                secondParCoord = self.GetParametricCoordinate(currentEdge, splitLine.second)
                if secondParCoord > tolDistance and secondParCoord < 1 - tolDistance:
                    isSecondOnEdge = True

            if isFirstOnEdge == True and isSecondOnEdge == True:
                if secondParCoord > firstParCoord:
                    polygon.insert(i + 1, splitLine.first)
                    polygon.insert(i + 2, splitLine.second)
                    break
                else:
                    polygon.insert(i + 1, splitLine.second)
                    polygon.insert(i + 2, splitLine.first)
                    break
            elif isFirstOnEdge == True:
                polygon.insert(i + 1, splitLine.first)
                i+=1
                size+=1
            elif isSecondOnEdge == True:
                polygon.insert(i + 1, splitLine.second)
                i+=1
                size+=1
            i+=1

    def ComputeArea(self, polygon: Vertices):
        matVertices = polygon
        matVertices.append(polygon[0])
        sumForward = 0
        for i in range(len(matVertices)-1):
            sumForward += matVertices[i].x * matVertices[i+1].y
        sumBack = 0
        for i in range(len(matVertices)-1):
            sumBack += matVertices[i].y * matVertices[i+1].x
        area = 0.5 * abs(sumForward - sumBack)
        return area

class ProblemFunctions:

    @staticmethod
    def CutPolygon(inputVertices: Vertices, inputPolygon: [int], splitLine: Vertices):
        polygon = []
        lengthInputPolygon = len(inputPolygon)
        for i in range(lengthInputPolygon):
            polygon.append(inputVertices[inputPolygon[i]])
        inputProblemData = InputProblemData(splitLine, polygon)

        isConcave = False
        for i in range(lengthInputPolygon):
            if AbstractPolygonCutter.GetVertexSide(Segment(inputProblemData.polygon[i], inputProblemData.polygon[(i + 1) % lengthInputPolygon]), inputProblemData.polygon[(i + 2) % lengthInputPolygon]) == SideOfVertex.RIGHT.value:
                isConcave = True
                break
        if isConcave == False:
            print("Used convex SplitPolygon")
            cutter = ConvexPolygonCutter(inputProblemData)
            return cutter.SplitPolygon()
        else:
            print("Used concave SplitPolygon")
            cutter = ConcavePolygonCutter(inputProblemData)
            return cutter.SplitPolygon()

class ConvexPolygonCutter(AbstractPolygonCutter):

    def __init__(self, inputProblemData: InputProblemData):
        self._inputProblemData = inputProblemData


    def SplitPolygon(self):
        lengthPolygon = len(self._inputProblemData.polygon)
        leftPolygon = []
        rightPolygon = []
        startingPolygon = self._inputProblemData.polygon
        splitLine = self._inputProblemData.splitLine
        output = OutputProblemData()
        # list containing all types of vertices (polygon vertices, splitline vertices and intersections)
        allVertices = self._inputProblemData.polygon
        # add the splitline vertices to allVertices
        allVertices.append(self._inputProblemData.splitLine.first)
        allVertices.append(self._inputProblemData.splitLine.second)

        current = startingPolygon[0]
        currentSide = AbstractPolygonCutter.GetVertexSide(splitLine, current)

        # itero sui lati del poligono e trovo i punti di intersezione tra i lati e la retta che taglia il poligono
        for i in range (1,lengthPolygon):
            next = startingPolygon[i%lengthPolygon]
            nextSide = AbstractPolygonCutter.GetVertexSide(splitLine, next)

            # check if the vertex is in the left or in th right polygon
            if currentSide!=SideOfVertex.ONLINE.value:
                if currentSide==SideOfVertex.LEFT.value:
                    leftPolygon.append(current)
                else:
                    rightPolygon.append(current)
                if (nextSide != SideOfVertex.ONLINE.value) and (currentSide != nextSide):
                    intersection = self.ComputeIntersection(splitLine, Segment(current,next), len(allVertices))
                    allVertices.append(intersection)
                    rightPolygon.append(intersection)
                    leftPolygon.append(intersection)
            else:
                rightPolygon.append(current)
                leftPolygon.append(current)
            current=next
            currentSide=nextSide

        leftSize = len(leftPolygon)
        rightSize = len(rightPolygon)

        if leftSize > 2:
            self.AddSplitLineVerticesToPolygon(leftPolygon,splitLine)
            output.polygons.append(leftPolygon)
        if rightSize > 2:
            self.AddSplitLineVerticesToPolygon(rightPolygon,splitLine)
            output.polygons.append(rightPolygon)

        output.vertices = allVertices

        return output

class ConcavePolygonCutter(AbstractPolygonCutter):
    def __init__(self, inputProblemData: InputProblemData):
        self._inputProblemData = inputProblemData

    def SplitPolygon(self):
        lengthPolygon = len(self._inputProblemData.polygon)
        # allVertices: lista contenente vertici del poligono, vertici del segmento e intersezioni tra poligono e segmento
        # splitPoly: lista di vertici del poligono e intersezioni ordinata percorrendo in senso antiorario il poligono
        # verticesOnLine : lista contenente i vertici che appartengono anche al segmento splitLine
        splitPoly: List[VertexPointer] = []
        verticesOnLine: List[int] = []

        allVertices = self._inputProblemData.polygon

        # aggiungo i vertici del segmento splitLine a allVertices
        # gli id del segmento devono essere successivi a quelli del poligono in input
        allVertices.append(self._inputProblemData.splitLine.first)
        allVertices.append(self._inputProblemData.splitLine.second)

        current = self._inputProblemData.polygon[0]
        currentSide = AbstractPolygonCutter.GetVertexSide(self._inputProblemData.splitLine, current)

        for i in range(1,lengthPolygon+1):
            next = self._inputProblemData.polygon[i % lengthPolygon]
            nextSide = AbstractPolygonCutter.GetVertexSide(self._inputProblemData.splitLine, next)
            splitPoly.append(VertexPointer(i-1))
            if currentSide == SideOfVertex.ONLINE.value :
                verticesOnLine.append(len(splitPoly)-1)
            elif currentSide != nextSide and nextSide != SideOfVertex.ONLINE.value:
                intersection = self.ComputeIntersection(self._inputProblemData.splitLine, Segment(current, next), len(allVertices))
                splitPoly.append(VertexPointer(intersection.id))
                verticesOnLine.append(len(splitPoly)-1)
                allVertices.append(intersection)
            current = next
            currentSide = nextSide


        #splitPolySize = len(splitPoly)
        for i in range(len(splitPoly)):
            splitPoly[i].prev = (i - 1 + len(splitPoly)) % len(splitPoly)
            splitPoly[i].next = (i + 1) % len(splitPoly)

        # calcolo coordinata parametrica
        for i in range(0,len(verticesOnLine)):
            currentPos = verticesOnLine[i]
            splitPoly[currentPos].parCoord = self.GetParametricCoordinate(self._inputProblemData.splitLine, allVertices[splitPoly[currentPos].id])

        # sort verticesOnLine
        verticesOnLine.sort(key=lambda x: getattr(splitPoly[x], 'parCoord'), reverse=False)

        alreadyFoundSource = False
        sourcePos = -1

        i=0
        while i < len(verticesOnLine):

            # find Source
            currPtr = VertexPointer(-1)
            foundSource = alreadyFoundSource
            alreadyFoundSource = False
            destinationPos = -1
            while foundSource == False and i < len(verticesOnLine) :
                currPtr = splitPoly[verticesOnLine[i]]
                prevSide = AbstractPolygonCutter.GetVertexSide(self._inputProblemData.splitLine, allVertices[splitPoly[currPtr.prev].id])
                nextSide = AbstractPolygonCutter.GetVertexSide(self._inputProblemData.splitLine, allVertices[splitPoly[currPtr.next].id])

                if (prevSide == SideOfVertex.LEFT.value and nextSide == SideOfVertex.RIGHT.value) or (prevSide == SideOfVertex.LEFT.value and nextSide == SideOfVertex.ONLINE.value and splitPoly[currPtr.next].parCoord < currPtr.parCoord) or (prevSide == SideOfVertex.ONLINE.value and nextSide == SideOfVertex.RIGHT.value and splitPoly[currPtr.prev].parCoord < currPtr.parCoord):
                    sourcePos = verticesOnLine[i]
                    foundSource = True
                else :
                    i += 1
            # find Destination
            foundDst = False
            while foundDst == False and i < len(verticesOnLine):
                currPtr = splitPoly[verticesOnLine[i]]
                prevSide = AbstractPolygonCutter.GetVertexSide(self._inputProblemData.splitLine,
                                                               allVertices[splitPoly[currPtr.prev].id])
                nextSide = AbstractPolygonCutter.GetVertexSide(self._inputProblemData.splitLine,
                                                               allVertices[splitPoly[currPtr.next].id])

                if (prevSide == SideOfVertex.RIGHT.value and nextSide == SideOfVertex.LEFT.value) or (prevSide == SideOfVertex.ONLINE.value and nextSide == SideOfVertex.LEFT.value) or (prevSide == SideOfVertex.RIGHT.value and nextSide == SideOfVertex.ONLINE.value) or (prevSide == SideOfVertex.RIGHT.value and nextSide == SideOfVertex.RIGHT.value) or (prevSide == SideOfVertex.LEFT.value and nextSide == SideOfVertex.LEFT.value):
                    destinationPos = verticesOnLine[i]
                    foundDst = True
                else:
                    i += 1

            # Double edges in common, and generate a new one
            if (foundDst == True and foundSource == True):
                a = VertexPointer(splitPoly[sourcePos].id)
                b = VertexPointer(splitPoly[destinationPos].id)
                a.next = destinationPos
                a.prev = splitPoly[sourcePos].prev
                b.next = sourcePos
                b.prev = splitPoly[destinationPos].prev
                splitPoly.append(a)
                splitPoly.append(b)
                splitPoly[splitPoly[sourcePos].prev].next = len(splitPoly)-2
                splitPoly[sourcePos].prev = len(splitPoly) - 1
                splitPoly[splitPoly[destinationPos].prev].next = len(splitPoly) - 1
                splitPoly[destinationPos].prev = len(splitPoly) - 2

                #check if destination is new source
                if AbstractPolygonCutter.GetVertexSide(self._inputProblemData.splitLine, allVertices[splitPoly[splitPoly[splitPoly[sourcePos].prev].prev].id]) == SideOfVertex.LEFT.value:
                    sourcePos = splitPoly[sourcePos].prev
                    alreadyFoundSource = True
                elif AbstractPolygonCutter.GetVertexSide(self._inputProblemData.splitLine, allVertices[splitPoly[splitPoly[destinationPos].next].id]) == SideOfVertex.RIGHT.value:
                    sourcePos = destinationPos
                    alreadyFoundSource = True
            i+=1

        output = OutputProblemData()


        for i in range(len(splitPoly)):
            if splitPoly[i].Visited == False:
                newPoly: Vertices = []
                currPos=i
                flag = True
                while flag == True:
                    splitPoly[currPos].Visited = True
                    newPoly.append(allVertices[splitPoly[currPos].id])
                    currPos = splitPoly[currPos].next
                    if currPos == i:
                        flag = False

                self.AddSplitLineVerticesToPolygon(newPoly,self._inputProblemData.splitLine)

                output.polygons.append(newPoly)

        for i in range(len(allVertices)):
            output.vertices.append(allVertices[i])
        return output


