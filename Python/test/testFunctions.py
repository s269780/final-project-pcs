from unittest import TestCase

import src.main as geometryLibrary


class AbstractPolygonCutter(TestCase):
    pointOneSegment = geometryLibrary.Vertex(2.0, 3.7,0)
    pointTwoSegment = geometryLibrary.Vertex(4.1, 5.9, 1)
    lineOne = geometryLibrary.Segment(pointOneSegment, pointTwoSegment)

    def test_GetSide(self):
        point1 = geometryLibrary.Vertex(1.5, 1.0, 0)
        point2 = geometryLibrary.Vertex(4.0, 6.2, 1)
        point3 = geometryLibrary.Vertex(4.20433,6.00929, 2)
        self.assertEqual(geometryLibrary.AbstractPolygonCutter.GetVertexSide(self.lineOne, point1), 1)
        self.assertEqual(geometryLibrary.AbstractPolygonCutter.GetVertexSide(self.lineOne, point2), 0)
        self.assertEqual(geometryLibrary.AbstractPolygonCutter.GetVertexSide(self.lineOne, point3), 2)

    def test_ComputeIntersection(self):
        pointOneLine = geometryLibrary.Vertex(1.5, 1.0, 0)
        pointTwoLine = geometryLibrary.Vertex(1.0, 4.0, 1)
        lineTwo = geometryLibrary.Segment(pointOneLine, pointTwoLine)
        pos = 2
        self.assertEqual(geometryLibrary.AbstractPolygonCutter.ComputeIntersection(self,self.lineOne, lineTwo, pos), geometryLibrary.Vertex(1.19121622, 2.8527027,2))

    def test_GetParametricCoordinate(self):
        segmentTest = geometryLibrary.Segment(geometryLibrary.Vertex(0.0, 0.0,0), geometryLibrary.Vertex(1.0, 1.0, 1))
        pointTest = geometryLibrary.Vertex(0.5, 0.5, 2)
        self.assertEqual(geometryLibrary.AbstractPolygonCutter.GetParametricCoordinate(self,segmentTest, pointTest), 0.5)

    def test_ComputeArea(self):
        point1 = geometryLibrary.Vertex(0.0, 0.0, 0)
        point2 = geometryLibrary.Vertex(1.0, 0.0, 1)
        point3 = geometryLibrary.Vertex(1.0, 1.0, 2)
        point4 = geometryLibrary.Vertex(0.0, 1.0, 3)
        polygon = [point1, point2, point3, point4]
        self.assertEqual(geometryLibrary.AbstractPolygonCutter.ComputeArea(self, polygon),1)
