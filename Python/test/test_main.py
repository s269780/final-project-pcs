from unittest import TestCase

import src.main as geometryLibrary



class ProblemFunctions(TestCase):

    # Test Rectangle
    def test_Rectangle(self):
        A = geometryLibrary.Vertex(2.0, 1.2, 4)
        B = geometryLibrary.Vertex(4.0, 3.0, 5)
        splitSegment = geometryLibrary.Segment(A, B)
        v0 = geometryLibrary.Vertex(1.0, 1.0, 0)
        v1 = geometryLibrary.Vertex(5.0, 1.0, 1)
        v2 = geometryLibrary.Vertex(5.0, 3.1, 2)
        v3 = geometryLibrary.Vertex(1.0, 3.1, 3)

        inputPoly = [0,1,2,3]
        vertices = [v0, v1, v2, v3, A, B]
        expected = geometryLibrary.OutputProblemData()
        v6 = geometryLibrary.Vertex(1.77777777,1,6)
        v7 = geometryLibrary.Vertex(4.11111111,3.1,7)
        expected.polygons = [[v0, v6, A, B, v7, v3],[v6, v1, v2, v7, B, A]]
        expected.vertices = [v0, v1, v2, v3, A, B, v6, v7]
        output = geometryLibrary.ProblemFunctions.CutPolygon(vertices, inputPoly, splitSegment)
        self.assertEqual(output, expected)

    # Test Pentagon
    def test_Pentagon(self):
        A = geometryLibrary.Vertex(1.4,2.75, 5)
        B = geometryLibrary.Vertex(3.6,2.2, 6)
        splitSegment = geometryLibrary.Segment(A, B)
        v0 = geometryLibrary.Vertex(2.5, 1.1, 0)
        v1 = geometryLibrary.Vertex(4,2.1,1)
        v2 = geometryLibrary.Vertex(3.4,4.2,2)
        v3 = geometryLibrary.Vertex(1.6,4.2,3)
        v4 = geometryLibrary.Vertex(1,2.1,4)

        inputPoly = [0,1,2,3,4]
        vertices = [v0, v1, v2, v3, v4, A, B]
        expected = geometryLibrary.OutputProblemData()
        v7 = geometryLibrary.Vertex(1.2,2.8,7)
        expected.polygons = [[v1, v2, v3, v7, A, B],[v0, v1, B, A, v7, v4]]
        expected.vertices = [v0, v1, v2, v3, v4, A, B, v7]
        output = geometryLibrary.ProblemFunctions.CutPolygon(vertices, inputPoly, splitSegment)
        self.assertEqual(output, expected)

    def test_Concave1(self):
        A = geometryLibrary.Vertex(2.0, 3.7,6)
        B = geometryLibrary.Vertex(4.1, 5.9, 7)
        splitSegment = geometryLibrary.Segment(A, B)
        v0 = geometryLibrary.Vertex(1.5, 1.0, 0)
        v1 = geometryLibrary.Vertex(5.6, 1.5, 1)
        v2 = geometryLibrary.Vertex(5.5, 4.8, 2)
        v3 = geometryLibrary.Vertex(4.0, 6.2, 3)
        v4 = geometryLibrary.Vertex(3.2, 4.2, 4)
        v5 = geometryLibrary.Vertex(1.0, 4.0, 5)
        inputPoly = [0,1,2,3,4,5]
        vertices = [v0, v1, v2, v3, v4, v5, A, B]
        expected = geometryLibrary.OutputProblemData()
        v8 = geometryLibrary.Vertex(4.20432692,6.00929487,8)
        v9 = geometryLibrary.Vertex(3.72131148,5.50327869,9)
        v10 = geometryLibrary.Vertex(2.40859729,4.1280543,10)
        v11 = geometryLibrary.Vertex(1.19121622,2.8527027,11)
        expected.polygons = [[v0,v1,v2,v8,B,v9,v4,v10,A,v11],[v8,v3,v9,B],[v10,v5,v11,A]]
        expected.vertices = [v0,v1,v2,v3,v4,v5,A,B,v8,v9,v10,v11]
        output = geometryLibrary.ProblemFunctions.CutPolygon(vertices, inputPoly, splitSegment)
        self.assertEqual(output, expected)

    def test_Convex(self):
        A = geometryLibrary.Vertex(4.0, 3.0 ,6)
        B = geometryLibrary.Vertex(7.0, 6.0, 7)
        splitSegment = geometryLibrary.Segment(A, B)
        v0 = geometryLibrary.Vertex(1.0, 1.0, 0)
        v1 = geometryLibrary.Vertex(7.0, 1.0, 1)
        v2 = geometryLibrary.Vertex(8.0, 3.0, 2)
        v3 = geometryLibrary.Vertex(6.0, 5.0, 3)
        v4 = geometryLibrary.Vertex(3.0, 8.0, 4)
        v5 = geometryLibrary.Vertex(0.0, 4.0, 5)
        v8 = geometryLibrary.Vertex(2.0, 1.0, 8)

        inputPoly = [0,1,2,3,4,5]
        vertices = [v0, v1, v2, v3, v4, v5, A, B]
        expected = geometryLibrary.OutputProblemData()

        expected.polygons = [[v0, v8, A, v3, v4, v5],[v8, v1, v2, v3, A]]
        expected.vertices = [v0, v1, v2, v3, v4, v5, A, B, v8]
        output = geometryLibrary.ProblemFunctions.CutPolygon(vertices, inputPoly, splitSegment)
        self.assertEqual(output, expected)

    def test_Concave2(self):
        A = geometryLibrary.Vertex(5.0, 5.0, 7)
        B = geometryLibrary.Vertex(3.0, 5.0, 8)
        splitSegment = geometryLibrary.Segment(A, B)
        v0 = geometryLibrary.Vertex(2.0, 2.0, 0)
        v1 = geometryLibrary.Vertex(5.0, 2.0, 1)
        v2 = geometryLibrary.Vertex(6.0, 3.0, 2)
        v3 = geometryLibrary.Vertex(5.0, 6.0, 3)
        v4 = geometryLibrary.Vertex(4.0, 5.0, 4)
        v5 = geometryLibrary.Vertex(3.0, 6.0, 5)
        v6 = geometryLibrary.Vertex(1.0, 3.0, 6)
        inputPoly = [0,1,2,3,4,5,6]
        vertices = [v0, v1, v2, v3, v4, v5, v6, A, B]
        expected = geometryLibrary.OutputProblemData()
        v9 = geometryLibrary.Vertex(5.333333333, 5, 9)
        v10 = geometryLibrary.Vertex(2.3333333333, 5, 10)
        expected.polygons = [[v0, v1, v2, v9, A, v4, B, v10, v6], [v9, v3, v4, A], [v4, v5, v10, B]]
        expected.vertices = [v0, v1, v2, v3, v4, v5, v6, A, B, v9, v10]
        output = geometryLibrary.ProblemFunctions.CutPolygon(vertices, inputPoly, splitSegment)
        self.assertEqual(output, expected)

    def test_extra1(self):
        A = geometryLibrary.Vertex(0.0, -3.0, 10)
        B = geometryLibrary.Vertex(0.0, 4.0, 11)
        splitSegment = geometryLibrary.Segment(A, B)
        v0 = geometryLibrary.Vertex(2.0, -2.0, 0)
        v1 = geometryLibrary.Vertex(0.0, -1.0, 1)
        v2 = geometryLibrary.Vertex(3.0, 1.0, 2)
        v3 = geometryLibrary.Vertex(0.0, 2.0, 3)
        v4 = geometryLibrary.Vertex(3.0, 2.0, 4)
        v5 = geometryLibrary.Vertex(3.0, 3.0, 5)
        v6 = geometryLibrary.Vertex(-1.0, 3.0, 6)
        v7 = geometryLibrary.Vertex(-3.0, 1.0, 7)
        v8 = geometryLibrary.Vertex(0.0, 0.0, 8)
        v9 = geometryLibrary.Vertex(-3.0, -2.0, 9)
        inputPoly = [0,1,2,3,4,5,6,7,8,9]
        vertices = [v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, A, B]
        expected = geometryLibrary.OutputProblemData()
        v12 = geometryLibrary.Vertex(0.0, 3.0, 12)
        v13 = geometryLibrary.Vertex(0.0, -2.0, 13)
        expected.polygons = [[v0, v1, v13],[ v1, v2, v3, v8],[ v3, v4, v5,v12], [v12, v6, v7, v8, v3], [v8, v9, v13, v1]]
        expected.vertices = [v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, A, B, v12, v13]
        output = geometryLibrary.ProblemFunctions.CutPolygon(vertices, inputPoly, splitSegment)
        self.assertEqual(output, expected)

    def test_extra2(self):
        A = geometryLibrary.Vertex(-4,-4,10)
        B = geometryLibrary.Vertex(4,4,11)
        splitSegment = geometryLibrary.Segment(A, B)
        v0 = geometryLibrary.Vertex(2.0, -2.0, 0)
        v1 = geometryLibrary.Vertex(0.0, -1.0, 1)
        v2 = geometryLibrary.Vertex(3.0, 1.0, 2)
        v3 = geometryLibrary.Vertex(0.0, 2.0, 3)
        v4 = geometryLibrary.Vertex(3.0, 2.0, 4)
        v5 = geometryLibrary.Vertex(3.0, 3.0, 5)
        v6 = geometryLibrary.Vertex(-1.0, 3.0, 6)
        v7 = geometryLibrary.Vertex(-3.0, 1.0, 7)
        v8 = geometryLibrary.Vertex(0.0, 0.0, 8)
        v9 = geometryLibrary.Vertex(-3.0, -2.0, 9)
        inputPoly = [0,1,2,3,4,5,6,7,8,9]
        vertices = [v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, A, B]
        expected = geometryLibrary.OutputProblemData()
        v12 = geometryLibrary.Vertex(1.5,1.5, 12)
        v13 = geometryLibrary.Vertex(2,2, 13)
        v14 = geometryLibrary.Vertex(-2,-2,14)
        expected.polygons = [[v0,v1,v2,v12,v8,v14],[v12,v3,v13,v5,v6,v7,v8],[v13,v4,v5],[v8,v9,v14]]
        expected.vertices = [v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, A, B, v12, v13, v14]
        output = geometryLibrary.ProblemFunctions.CutPolygon(vertices, inputPoly, splitSegment)
        self.assertEqual(output, expected)



